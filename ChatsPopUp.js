// document.addEventListener("DOMContentLoaded", function(event) {
// import SocialMedias from './SocialMedias.js';

class chatsPopUp
{
    MainClass;
    elClass;
    elC = []
    elChats = [];
    duration;

    setElChats(...el)
    {
        for(let i = 0; i < el.length; i++) {
            this._setElChat = el[i];
        }
        console.log(this.duration)
    }

    checkExist(element)
    {
        return new Promise((resolve, reject) => {
            let interval = setInterval(function() {
                if (element) {
                    clearInterval(interval);
                    return resolve();
                }else{
                    console.log('elemento ainda não existe');
                }
            }, 100);
        });
    }

    // Cria os elementos na tela, seta a largura e alinha verticalmente
    _createEl()
    {
        this.getElClass.insertAdjacentHTML('beforeend', '<ul class="chatsPopUpUl"></ul>');
        let ul = document.querySelector('.chatsPopUpUl');

        for(let i = 0; i < this.getElChats.length; i++) {
            ul.insertAdjacentHTML('beforeend', `<li><i class="icofont-${this.getElChats[i]} SMElPopUp SMElPopUp-${this.getElChats[i]}"></i></li>`); 
            this.elC.push(document.querySelector(`.SMElPopUp-${this.getElChats[i]}`));
        }

        // Definindo a largura total do slider
        ul.style.width = (this.getElChats.length) * 100 + '%';
        // Centralizando verticalmente elementos na div
        this.checkExist(ul).then( e => {
            ul.style.marginTop = -ul.offsetHeight/2 + 'px';
        });

        //Chamando para deslizar
        this._slideEl();
    }


    // Faz os elementos ficarem em slide
    _slideEl()
    {

        let slide = 0;
        let arr = [
        { transform: `translateX(${slide}%)` },
        { transform: `translateX(${slide}%)`, easing: 'cubic-bezier(0.680, -0.550, 0.265, 1.550)' }
        ];
        let slidePercentage = 100/this.getElChats.length;
        for(let i = 0; i < this.getElChats.length-1; i++) {
            slide -= slidePercentage;
            arr.push({ transform: `translateX(${slide}%)`, easing: 'cubic-bezier(0.680, -0.550, 0.265, 1.550)'});
            arr.push({ transform: `translateX(${slide}%)`, easing: 'cubic-bezier(0.680, -0.550, 0.265, 1.550)'});
        }
        arr.push({ transform: 'translateX(0%)', easing: 'cubic-bezier(0.680, -0.550, 0.265, 1.550)'});

        document.querySelector('.chatsPopUpUl').animate(arr,{    
            duration: this.duration,
            iterations: Infinity,
        });
    }


    // Métodos Especiais
    constructor(classe, duration, ...chats) 
    {
	   this.setMainClasse = classe;
       this.setElClasse = classe;
       this.setDuration = duration;
       this.setElChats(...chats);
       this._createEl();
    }

    get getElClass() 
    {
        return this.elClass;
    }

    get getElC() 
    {
        return this.elC;
    }

    set setElClasse(classe) 
    {
        return this.elClass = document.querySelector(`.${classe}`);
    }        

    get getMainClass() 
    {
        return this.MainClass;
    }

    set setMainClasse(classe) 
    {
        return this.MainClass = classe;
    }

    get getElChats()
    {
        return this.elChats;
    }

    set _setElChat(elChats)
    {
        return this.elChats.push(elChats);
    }

    get getDuration() 
    {
        return this.duration;
    }

    set setDuration(duration) 
    {
        return this.duration = duration;
    }
}
// });